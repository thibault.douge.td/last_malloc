#include "malloc.h"

int 	liberate_tiny(t_tiny *tiny, t_tiny *tmp)
{
	int i;

	i = 0;
	while (i < 128)
	{
		if (tiny->use[i] == 1)
			return 0;
		i++;
	}
	if (tmp == tiny)
	{
		g_env.tiny = tiny->next;
		munmap(tiny, tiny->length + sizeof(*tiny));
		return 1;
	}
	else
	{
		while (tmp->next != tiny)
			tmp = tmp->next;
		tmp->next = tiny->next;
		munmap(tiny, tiny->length + sizeof(*tiny));
		return 1;
	}
	return 0;
}

int 	liberate_large(t_large *large, void *ptr)
{
	if (ptr == (void *)large + sizeof(*large))
	{
		g_env.large = large->next;
		munmap(large, large->length + sizeof(*large));
		return 1;
	}
	else
	{
		while (large != NULL && (void *)large->next + sizeof(*large) != ptr)
			large = large->next;
		if (large->next != NULL)
			large->next = large->next->next;
		else
			large->next = NULL;
		munmap(large, large->length + sizeof(*large));
		return 1;
	}
	return 0;
}


int 	liberate_small(t_small *small, t_small *tmp)
{
	int i;

	i = 0;
	while (i < 128)
	{
		if (small->use[i] == 1)
			return 0;
		i++;
	}
	if (tmp == small)
	{
		g_env.small = small->next;
		munmap(small, small->length + sizeof(*small));
		return 1;
	}
	else
	{
		while (tmp->next != small)
			tmp = tmp->next;
		&tmp->next = &small->next;
		munmap(small, small->length + sizeof(*small));
		return 1;
	}
	return 0;
}

void test_tiny(void *ptr, t_tiny *tiny)
{
	int i = 0;

	t_tiny *tmp;
	tmp = tiny;
	while (tiny != NULL)
	{
		if (ptr >= (void *)tiny + sizeof(*tiny) && ptr < (void *)tiny + sizeof(*tiny) + tiny->length)
		{
			while (i < 128)
			{
				if (ptr == (void *)tiny + sizeof(*tiny) + i * TINY)
				{
					tiny->use[i] = 0;
					if (liberate_tiny(tiny, tmp) == 1)
						return;
				}
				i++;
			}
		}
		tiny = tiny->next;
	}
}


void test_small(void *ptr, t_small *small)
{
	int i = 0;

	t_small *tmp;
	tmp = small;
	while (small != NULL)
	{
		if (ptr >= (void *)small + sizeof(*small) && ptr < (void *)small + sizeof(*small) + small->length)
		{
			while (i < 128)
			{
				if (ptr == (void *)small + sizeof(*small) + i * SMALL)
				{
					small->use[i] = 0;
					if (liberate_small(small, tmp) == 1)
						return;
				}
				i++;
			}
		}
		small = small->next;
	}
}

void 	free(void *ptr)
{
	test_tiny(ptr, g_env.tiny);
	test_small(ptr, g_env.small);
	liberate_large(g_env.large, ptr);
}