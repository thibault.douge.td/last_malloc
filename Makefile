.PHONY: libft

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME :=	libft_malloc_$(HOSTTYPE).so

MALLOCLIB :=	libft_malloc.so

SOFLAGS	:=	-shared

SRC_DIR = src

SRC_FILE =	malloc.c free.c create_container.c realloc.c

SRC = $(addprefix $(SRC_DIR)/, $(SRC_FILE))

OBJS_DIR = obj

OBJS = $(addprefix $(OBJS_DIR)/, $(SRC_FILE:.c=.o))

INC = -ILibft/include -Iinclude

LFT = -L./Libft -lft

all : libft $(NAME) link_malloc

$(NAME): $(OBJS)
	@gcc $(SOFLAGS) $(LFT) -o $(NAME) $(OBJS)
	@echo "\033[32mexecutable malloc créé\033[0m"

libft:
	make -C Libft/

link_malloc:
	rm -f $(MALLOCLIB)
	ln -s $(NAME) $(MALLOCLIB)

$(OBJS_DIR)/%.o: $(SRC_DIR)/%.c
	@mkdir -p $(OBJS_DIR)
	@gcc $(INC) -o $@ -c $<
	@echo "Fichier" $< "recompilé."

clean:
	@rm -rf $(OBJS_DIR)
	@make -C Libft clean
	@echo "\033[31mFichier objet de malloc supprimé\033[0m"

fclean: clean
	@rm -f $(NAME)
	@rm -f Libft/libft.a
	@echo "\033[31mLibft.a supprimé\033[0m"
	@echo "\033[31mmalloc supprimé\033[0m"

re: fclean all
