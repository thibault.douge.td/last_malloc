#include "../include/malloc.h"

t_tiny	*container_tiny(void)
{
	t_tiny *new_container;
	size_t length;

	length = TINY * ALL_BLOCK;
	length = ((length + sizeof(*g_env.tiny) - 1) /
			getpagesize()) * getpagesize() + getpagesize();
	if ((new_container = mmap(0, length, PROT_READ | PROT_WRITE, MAP_ANON
					| MAP_PRIVATE, -1, 0)) == MAP_FAILED)
		return (NULL);
	new_container->length = length - sizeof(*g_env.tiny);
	new_container->next = NULL;
	return (new_container);
}

t_small	*container_small(void)
{
	t_small *new_container;
	size_t length;

	length = SMALL * ALL_BLOCK;
	length = ((length + sizeof(*g_env.small) - 1) /
			getpagesize()) * getpagesize() + getpagesize();
	if ((new_container = mmap(0, length, PROT_READ | PROT_WRITE, MAP_ANON
					| MAP_PRIVATE, -1, 0)) == MAP_FAILED)
		return (NULL);
	new_container->length = length - sizeof(*g_env.small);
	new_container->next = NULL;
	return (new_container);
}

t_large *container_large(size_t length)
{
	t_large *new_container;


	length = ((length + sizeof(*g_env.large) - 1) /
			getpagesize()) * getpagesize() + getpagesize();
	if ((new_container = mmap(0, length, PROT_READ | PROT_WRITE, MAP_ANON
					| MAP_PRIVATE, -1, 0)) == MAP_FAILED)
		return (NULL);
	new_container->length = length - sizeof(*g_env.large);
	new_container->next = NULL;
	return (new_container);
}
