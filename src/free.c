#include "../include/malloc.h"

int 	liberate_tiny(t_tiny *tiny, t_tiny *tmp)
{
	int i;

	i = 0;
	while (i < 128)
	{
		if (tiny->use[i] == 1)
			return 0;
		i++;
	}
	if (tmp == tiny)
	{
		g_env.tiny = tiny->next;
		munmap(tiny, tiny->length + sizeof(*tiny));
		return 1;
	}
	else
	{
		while (g_env.tiny->next != tiny)
			g_env.tiny = g_env.tiny->next;
		g_env.tiny->next = tiny->next;
		munmap(tiny, tiny->length + sizeof(*tiny));
		g_env.tiny = tmp;
		return 1;
	}

	return 0;
}

int 	liberate_small(t_small *small, t_small *tmp)
{
	int i;

	i = 0;
	while (i < 128)
	{
		if (small->use[i] == 1)
			return 0;
		i++;
	}
	if (tmp == small)
	{
		g_env.small = small->next;
		munmap(small, small->length + sizeof(*small));
		return 1;
	}
	else
	{
		while (g_env.small->next != small)
			g_env.small = g_env.small->next;
		g_env.small->next = small->next;
		munmap(small, small->length + sizeof(*small));
		g_env.small = tmp;
		return 1;
	}
	return 0;
}

int 	liberate_large(void *ptr)
{
	t_large *tmp = g_env.large;
	t_large *tmp2;

	if (g_env.large)
	{
		if ((void *)g_env.large + sizeof(*g_env.large) == ptr)
		{
			tmp = tmp->next;
			munmap(g_env.large, g_env.large->length + sizeof(*g_env.large));
			g_env.large = tmp;
			return 1;
		}
		else
		{
			while (g_env.large->next != NULL && (void *)g_env.large->next + sizeof(*g_env.large->next) != ptr)
				g_env.large = g_env.large->next;
			if ((void*)g_env.large->next + sizeof(*g_env.large->next) == ptr)
			{
				tmp2 = g_env.large->next->next;
				munmap(g_env.large->next, g_env.large->next->length + sizeof(*g_env.large->next));
				g_env.large->next = tmp2;
				g_env.large = tmp;

				return 1;
			}


		}
	}
				g_env.large = tmp;
	
	return 0;
}


int 	test_tiny(void *ptr, t_tiny *tiny)
{
	int i = 0;

	t_tiny *tmp;
	tmp = tiny;
	while (tiny != NULL)
	{
		if (ptr >= (void *)tiny + sizeof(*tiny) && ptr < (void *)tiny + sizeof(*tiny) + tiny->length)
		{
			while (i < 128)
			{
				if (ptr == (void *)tiny + sizeof(*tiny) + i * TINY)
				{
					tiny->use[i] = 0;
					if (liberate_tiny(tiny, tmp) == 1)
						return 1;
				}
				i++;
			}
		}
		i = 0;
		tiny = tiny->next;
	}
	return 0;
}


int	test_small(void *ptr, t_small *small)
{
	int i = 0;

	t_small *tmp;
	tmp = small;
	while (small != NULL)
	{
		if (ptr >= (void *)small + sizeof(*small) && ptr < (void *)small + sizeof(*small) + small->length)
		{
			while (i < 128)
			{
				if (ptr == (void *)small + sizeof(*small) + i * SMALL)
				{
					small->use[i] = 0;
					if (liberate_small(small, tmp) == 1)
						return 1;
				}
				i++;
			}
		}
		i = 0;
		small = small->next;
	}
	return 0;
}

void 	free(void *ptr)
{
	
	if (test_tiny(ptr, g_env.tiny) == 1)
		return ;
	if (test_small(ptr, g_env.small) == 1)
		return;
	else
	{
		liberate_large(ptr);
		return ;
	}
}