#include "../include/malloc.h"

void	*realloc_tiny(void *ptr, t_tiny *tiny, size_t size)
{
	int i = 0;
	void *new;

	t_tiny *tmp;
	tmp = tiny;
	while (tiny != NULL)
	{
		if (ptr >= (void *)tiny + sizeof(*tiny) && ptr < (void *)tiny + sizeof(*tiny) + tiny->length)
		{
			while (i < 128)
			{
				if (ptr == (void *)tiny + sizeof(*tiny) + i * TINY)			
				{
					if (size > TINY)
					{
						new = malloc(size);
						new = ft_memcpy(new, (void *)tiny + sizeof(*tiny) + i * TINY, TINY);
						free(ptr);
						return new;
					}
					return ptr;
				}
				i++;
			}
		}
		i = 0;
		tiny = tiny->next;
	}
	return 0;
}

void	*realloc_small(void *ptr, t_small *small, size_t size)
{
	int i = 0;
	void *new;

	t_small *tmp;
	tmp = small;
	while (small != NULL)
	{
		if (ptr >= (void *)small + sizeof(*small) && ptr < (void *)small + sizeof(*small) + small->length)
		{
			while (i < 128)
			{
				if (ptr == (void *)small + sizeof(*small) + i * SMALL)
				{
					if (size > SMALL)
					{
						new = malloc(size);
						new = ft_memcpy(new, (void *)small + sizeof(*small) + i * SMALL, SMALL);
						free(ptr);
						return new;
					}
					return ptr;
				}
				i++;
			}
		}
		i = 0;
		small = small->next;
	}
	return NULL;
}

void 	*realloc_large(void *ptr, t_large *large, size_t size)
{
	void *new;
	while (large != NULL)
	{
		if (ptr == (void *)large + sizeof(*large))
		{
			if (size > large->length)
			{
				new = malloc(size);
				new = ft_memcpy(new, (void *)large + sizeof(*large), large->length);
				free(ptr);
				return new;
			}
			return ptr;
		}
		large = large->next;
	}
	return NULL;
}

void	*test_ptr(void *ptr, size_t size)
{
	void *tmp;
	if ((tmp = realloc_large(ptr, g_env.large, size)) != NULL)
	{
		return tmp;
	}
	else if ((tmp = realloc_small(ptr, g_env.small, size)) != NULL)
	{
		return tmp;
	}
	else if ((tmp = realloc_tiny(ptr, g_env.tiny, size)) != NULL)
	{
		return tmp;
	}
	return NULL;
}

void *realloc(void *ptr, size_t size)
{
	if (size <= 0 && ptr == NULL)
	{
		return NULL;
	}
	if (size <= 0)
	{
		return ptr;
	}
	if (ptr == NULL)
		return malloc(size);
	return test_ptr(ptr, size);
	// free(ptr);
	// return malloc(size);
}