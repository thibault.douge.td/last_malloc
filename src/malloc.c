#include "../include/malloc.h"
t_env g_env = {NULL, NULL, NULL};
void *tiny(t_tiny *tiny)
{
	int i;

	i = 0;
	while (tiny->use[i] == 1)
	{
		if (i == ALL_BLOCK - 1)
		{
			if (tiny->next != NULL)
			{
				i = 0;
				tiny = tiny->next;
			}
			else
			{
				tiny->next = container_tiny();
				tiny = tiny->next;
				i = 0;
				break;
			}
		}
		i++;
	}
	tiny->use[i] = 1;
	return ((void *)tiny + sizeof(*tiny) + i * TINY);
}

void *small(t_small *small)
{
	int i;

	i = 0;
	while (small->use[i] == 1)
	{
		if (i == ALL_BLOCK - 1)
		{
			if (small->next != NULL)
			{
				i = 0;
				small = small->next;
			}
			else
			{
				small->next = container_small();
				small = small->next;
				i = 0;
				break ;
			}
		}
		i++;
	}
	small->use[i] = 1;
	return (void *)small + sizeof(*small) + i * SMALL;
}

void	*large(t_large *large, size_t size)
{
	while (large->next != NULL)
		large = large->next;
	large->next = container_large(size);
	large = large->next;
	return (void *)large + sizeof(*large);
}

void	*malloc(size_t size)
{

	if (size <= TINY)
	{
		if(!g_env.tiny)
			g_env.tiny = container_tiny();
		return (tiny(g_env.tiny));
	}
	else if (size <= SMALL)
	{

		if(!g_env.small)
			g_env.small = container_small();
		return (small(g_env.small));
	}	
	else if (size > SMALL)
	{
		if(!g_env.large)
		{
			g_env.large = container_large(size);
			return (void *)g_env.large + sizeof(*g_env.large);
		}
		return (large(g_env.large, size));
	}
	return NULL;
}
