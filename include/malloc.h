#ifndef MALLOC_H
# define MALLOC_H
# include <stdio.h>
# include <unistd.h>
# include <sys/mman.h>
# include "../libft/include/libft.h"
# define TINY 64
# define SMALL 1024
# define ALL_BLOCK 128

void free(void *ptr);
void *malloc(size_t size);
void *realloc(void *ptr, size_t size);

typedef struct s_tiny
{
	int use[ALL_BLOCK];
	size_t length;
	struct s_tiny *next;

}		t_tiny;

typedef struct s_small
{
	int use[ALL_BLOCK];
	size_t length;
	struct s_small *next;
}		t_small;

typedef struct s_large
{
	size_t length;
	struct s_large *next;
}		t_large;


typedef struct			s_env
{
	t_tiny				*tiny;
	t_small				*small;
	t_large				*large;
}						t_env;



extern t_env			g_env;

t_tiny	*container_tiny(void);
t_small	*container_small(void);
t_large *container_large(size_t length);
void	*malloc(size_t size);
void 	free(void *ptr);
void 	*realloc(void *ptr, size_t size);
// void	show_alloc_mem();

#endif